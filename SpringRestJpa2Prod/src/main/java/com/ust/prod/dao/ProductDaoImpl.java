package com.ust.prod.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.ust.prod.Entity.Product;
import com.ust.prod.exception.ProductException;
import com.ust.prod.util.JPAUtil;

@Repository("pDao")
public class ProductDaoImpl implements ProductDao {

	@Override
	public Product findProduct(Integer prodId) {
		Product prod = null;
		EntityManager em = JPAUtil.getEntityManager();
		prod = em.find(Product.class, prodId);
		System.out.println(" ********* **** "+prod);
		if(prod==null) {			
			throw new ProductException("Failed to find Product");
		}
		return prod;
	}

	@Override
	public Product addProduct(Product prod) {
		try {
			prod.setProdId(0);
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(prod);
		em.getTransaction().commit();
		}catch(Exception e) {
			e.printStackTrace();
			throw new ProductException("Failed to save Product");
		}
		return prod;
	}

	@Override
	public List<Product> getAllProduct() {
		EntityManager em = JPAUtil.getEntityManager();
		String qry = "Select p from Product p";
		TypedQuery<Product> querry = em.createQuery(qry, Product.class);
		List<Product> prodList = querry.getResultList();
		return prodList;
	}

	@Override
	public Product updateProduct(Product prod) {
//		System.out.println(" ********Prod*******" + prod);
		Product updatedProd = null;
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		updatedProd = em.find(Product.class, prod.getProdId());
//		System.out.println("***** Updated Product  **********"+ updatedProd);
		if(updatedProd!=null) {
			updatedProd.copy(prod);
			em.merge(updatedProd);
			em.getTransaction().commit();
		}
		return updatedProd;
	}

	@Override
	public Product deleteProduct(Integer prodId) {
		Product deletedProd = null;
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		deletedProd = em.find(Product.class, prodId);
		if(deletedProd!=null) {
			em.remove(deletedProd);
			em.getTransaction().commit();
		}
		return deletedProd;
	}

}
