package com.ust.prod.Entity;
/*
 {"prodId":1001,
 "prodName":"Cheese",
 "quantity":10,
 "price":200.0,
 "expiryDate":"31-Jul-2021"}
 */

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "prod_detail_spring")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int prodId;
	private String prodName;
	private int quantity;
	private double price;
	@JsonFormat(pattern = "dd-MMM-yyyy",  timezone = "Asia/Calcutta")
	private Date expiryDate;
	
	public Product() {}

	public Product(int prodId, String prodName, int quantity, double price, Date expiryDate) {
		super();
		this.prodId = prodId;
		this.prodName = prodName;
		this.quantity = quantity;
		this.price = price;
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", quantity=" + quantity + ", price=" + price
				+ ", expiryDate=" + expiryDate + "]";
	}

	public int getProdId() {
		return prodId;
	}

	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void copy(Product prod) {
		prodId = prod.getProdId();
		prodName = prod.getProdName();
		quantity = prod.getQuantity();
		price = prod.getPrice();
		expiryDate = new Date(prod.getExpiryDate().getTime());// safe copy, immutable
		
	}
	
	
	
}
