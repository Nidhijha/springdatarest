package com.ust.prod.exception;

public class ProductException extends RuntimeException {

	public ProductException(String msg) {
		super(msg);
	}
}
