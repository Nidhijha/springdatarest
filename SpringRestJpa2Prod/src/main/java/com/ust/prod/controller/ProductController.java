package com.ust.prod.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ust.prod.Entity.Product;
import com.ust.prod.exception.ProductException;
import com.ust.prod.service.ProductService;

@RestController
@RequestMapping("/prod")
public class ProductController {

	@Autowired
	private ProductService pService;
	
	// Find
	@GetMapping("/{prodid}")
	public Product findProduct(@PathVariable("prodid") Integer prodId){
		Product prod = pService.findProduct(prodId);
		return prod;
		}
	
	//ADD
	@PostMapping("/add")
	public Product addProduct(@RequestBody Product prod) {
		Product addedProd = pService.addProduct(prod);
		return addedProd;
	}
	
	//FinDALL
	@GetMapping("/all")
	public List<Product> getAllProduct(){
		List<Product> prodList = pService.getAllProduct();
		return prodList;
	}
	
	//Update
	@PutMapping("/update")
	public Product updateProduct(@RequestBody Product prod) {
		System.out.println("in update *******" + prod);
		Product updatedProd = pService.updateProduct(prod);
		return updatedProd;
	}
	
	//DELETE
	@DeleteMapping("/delete/{prodid}")
	public Product deleteProduct(@PathVariable("prodid") Integer prodId) {
		Product deletedProd = pService.deleteProduct(prodId);
		return deletedProd;		
	}
}
