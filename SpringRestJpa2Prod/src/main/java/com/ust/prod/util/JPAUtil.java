package com.ust.prod.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUtil {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	public static EntityManager getEntityManager() {
		try {
			if (em == null || !em.isOpen()) {                              //prodentity
				emf = Persistence.createEntityManagerFactory("prodentity");
				em = emf.createEntityManager();
			}
		} catch (Exception e) {
		}
		return em;
	}

	public static void close() {
		if (em != null)
			em.close();
	}
}
