package com.ust.prod.service;

import java.util.List;

import com.ust.prod.Entity.Product;

public interface ProductService {

	Product findProduct(Integer prodId);

	Product addProduct(Product prod);

	List<Product> getAllProduct();

	Product updateProduct(Product prod);

	Product deleteProduct(Integer prodId);

}
