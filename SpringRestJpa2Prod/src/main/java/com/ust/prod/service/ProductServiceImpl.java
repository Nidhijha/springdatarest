package com.ust.prod.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.prod.Entity.Product;
import com.ust.prod.dao.ProductDao;

@Service("pService")
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao pDao;
	
	@Override
	public Product findProduct(Integer prodId) {
//		return new Product(10006,"TV",5,55000,new Date(2021, 10, 11));
		return pDao.findProduct(prodId);
	}

	@Override
	public Product addProduct(Product prod) {
		return pDao.addProduct(prod);
	}

	@Override
	public List<Product> getAllProduct() {
		return pDao.getAllProduct();
	}

	@Override
	public Product updateProduct(Product prod) {
		return pDao.updateProduct(prod);
	}

	@Override
	public Product deleteProduct(Integer prodId) {
		return  pDao.deleteProduct(prodId);
	}

}
